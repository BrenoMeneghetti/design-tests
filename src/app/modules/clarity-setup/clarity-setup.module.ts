import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ClarityModule, ClrIconModule } from '@clr/angular';

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    ClarityModule,
    ClrIconModule
  ],
  exports: [
    ClrIconModule
  ]
})
export class ClaritySetupModule { }
