import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ClarityUXRoutingModule } from './clarity-ux-routing.module';
import { HeaderComponent } from './components/header/header.component';
import { HomeComponent } from './container/home/home.component';
import { ProfilesComponent } from './container/profiles/profiles.component';
import { ClarityModule } from '@clr/angular';
import { ClaritySetupModule } from '../clarity-setup/clarity-setup.module';
import { SharedModule } from '../shared/shared.module';

@NgModule({
  declarations: [HeaderComponent, HomeComponent, ProfilesComponent],
  imports: [
    CommonModule,
    ClaritySetupModule,
    SharedModule,
    ClarityUXRoutingModule
  ]
})
export class ClarityUXModule { }
