import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './container/home/home.component';
import { ProfilesComponent } from './container/profiles/profiles.component';

const routes: Routes = [
  {
    path: 'clarity',
    children: [
      { path: '', component: HomeComponent },
      {
        path: 'profiles',
        component: ProfilesComponent
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ClarityUXRoutingModule {}
